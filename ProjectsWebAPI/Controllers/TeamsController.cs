﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<List<TeamDTO>> Get()
        {
            return Ok(_teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            return Ok(_teamService.GetById(id));
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _teamService.Delete(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO team)
        {
            try
            {
                _teamService.Create(team);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPut]
        public ActionResult Put(int id, [FromBody] TeamDTO team)
        {
            try
            {
                _teamService.Update(id, team);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }
    }
}
