﻿using Newtonsoft.Json;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectConsole.Services
{
    public class TeamService
    {
        private readonly HttpClient _client = new HttpClient();
        const string Alias = "https://localhost:44320/api/Teams/";
        public async Task<string> AddTeamAsync()
        {
            

            TeamDTO team = new TeamDTO();

            Console.WriteLine("Enter team Name: ");
            team.Name = Console.ReadLine();
            team.CreatedAt = DateTime.Now;

            HttpResponseMessage response = await _client.PostAsync(Alias, new StringContent(System.Text.Json.JsonSerializer.Serialize(team), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            return temp;

        }

        internal async Task<List<TeamDTO>> ShowTeamsAsync()
        {
            HttpResponseMessage response = await _client.GetAsync(Alias);
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<TeamDTO> teams = JsonConvert.DeserializeObject<List<TeamDTO>>(temp);
            return teams;
        }

        public async Task FindTeamById()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.GetAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            TeamDTO team = JsonConvert.DeserializeObject<TeamDTO>(temp);
            if (team != null)
            {
                Console.WriteLine(temp);
            }

        }

        internal async Task DeleteTeamAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.DeleteAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            TeamDTO team = JsonConvert.DeserializeObject<TeamDTO>(temp);
            if (team != null)
            {
                Console.WriteLine(temp);
            }
        }
    }
}
