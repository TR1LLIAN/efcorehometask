﻿using Newtonsoft.Json;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectConsole.Services
{
    public class TaskService
    {
        private readonly HttpClient _client = new HttpClient();
        const string Alias = "https://localhost:44320/api/Tasks/";
        public async Task<string> AddTaskAsync()
        {
            Console.WriteLine("Enter task : ");
            TaskDTO task = new TaskDTO();
            task.Name = Console.ReadLine();
            Console.Write("Enter performerID ");
            task.PerformerId = Convert.ToInt32( Console.ReadLine());
            Console.Write("Enter TaskID ");
            task.ProjectId = Convert.ToInt32(Console.ReadLine());

            task.CreatedAt = DateTime.Now;
            Console.WriteLine("Enter task decsription");
            task.Description = Console.ReadLine();

            HttpResponseMessage response = await _client.PostAsync(Alias, new StringContent(System.Text.Json.JsonSerializer.Serialize(task), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            return temp;

        }

        internal async Task<List<TaskDTO>> ShowTasksAsync()
        {
            HttpResponseMessage response = await _client.GetAsync(Alias);
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<TaskDTO> tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(temp);
            return tasks;
        }

        public async Task FindTaskById()
        {
            Console.WriteLine("Enter ID:");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.GetAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            TaskDTO task = JsonConvert.DeserializeObject<TaskDTO>(temp);
            if (task != null)
            {
                Console.WriteLine(temp);
            }

        }

        internal async Task DeleteTaskAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.DeleteAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            TaskDTO task = JsonConvert.DeserializeObject<TaskDTO>(temp);
            if (task != null)
            {
                Console.WriteLine(temp);
            }
        }
    }
}
