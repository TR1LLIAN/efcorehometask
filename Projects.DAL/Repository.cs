﻿using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.DAL
{
    public class Repository<T> : IRepository<T> where T : BaseModel
    {
        private readonly ProjectDbContext _items;

        public Repository(ProjectDbContext items)
        {
            _items = items;
        }

        public void Create(T item)
        {
            var entityType = _items.Model.FindEntityType(typeof(T));
            var tableName = entityType.GetTableName();
            if (_items.Set<T>().Contains(item))
            {
                throw new InvalidOperationException("Such entry already exist!");
            }
            else
            {
                _items.Set<T>().Add(item);
                _items.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            T deletedItem = _items.Set<T>().FirstOrDefault(it => it.Id == id);
            if(deletedItem is null)
            {
                throw new InvalidOperationException("Cannot delete not existing item!");
            }
            _items.Set<T>().Remove(deletedItem);
            _items.SaveChanges();
        }

        public T GetItemById(int id)
        {
            return _items.Set<T>().FirstOrDefault(it => it.Id == id);
        }

        public List<T> GetList()
        {
            return _items.Set<T>().ToList();
        }

        public void Update(int id,T item)
        {
            var entity = _items.Set<T>().AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (entity != null)
            {
                entity = item;
                entity.Id = id;
                _items.Set<T>().Update(entity);
            }
            _items.SaveChanges();
        }
    }
}
