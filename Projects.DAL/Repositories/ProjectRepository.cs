﻿using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;

namespace Projects.DAL.Repositories
{
    public class ProjectRepository:Repository<Project>, IProjectRepository
    {
      
        public ProjectRepository(ProjectDbContext projects) : base(projects)
        {
            
        }
    }
}
