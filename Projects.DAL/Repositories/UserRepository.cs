﻿using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;

namespace Projects.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ProjectDbContext users) : base(users)
        {
            
        }
    }
}
