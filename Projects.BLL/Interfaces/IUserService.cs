﻿using Projects.Common.DTOmodels;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface IUserService
    {
        List<UserDTO> GetUsers();
        void Delete(int id);
        void Create(UserDTO user);
        void Update(int id,UserDTO user);
        UserDTO GetById(int id);
    }
}
