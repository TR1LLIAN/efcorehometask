﻿using Projects.Common.DTOmodels;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ITeamService
    {
        List<TeamDTO> GetTeams();
        void Delete(int id);
        void Create(TeamDTO team);
        void Update(int id,TeamDTO team);
        TeamDTO GetById(int id);
    }
}
