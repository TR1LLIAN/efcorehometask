﻿using Projects.Common.DTOmodels;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ITaskService
    {
        List<TaskDTO> GetTasks();
        void Delete(int id);
        void Create(TaskDTO task);
        void Update(int id,TaskDTO task);
        TaskDTO GetById(int id);
    }
}
