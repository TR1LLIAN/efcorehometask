﻿using AutoMapper;
using Projects.Common.DTOmodels;
using Projects.DAL.Models;

namespace Projects.BLL.Maping
{
    public class MapTeam : Profile
    {
        public MapTeam()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>().ForMember(src => src.Id, opts => opts.Ignore()); 

        }
    }
}
